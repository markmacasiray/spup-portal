<footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Saint Paul University Philippines</h5>
                <p class="grey-text text-lighten-4">Mabini Street, Tuguegarao City, <span><br>Cagayan, 3500 Philippines</span></p>
                <p class="grey-text text-lighten-4">(+632) 396-1987 to 1994</p>
                <p class="grey-text text-lighten-4">(078) 846-4305</p>
                <p class="grey-text text-lighten-4">spupadmin@spup.edu.ph</p>

              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Follow us</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2017 markmacasiray
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>
            

    <!-Import jQuery before materialize.js-!>
    <script type="text/javascript" src="./lib/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="./lib/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="./lib/materialize/js/materialize.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.parallax').parallax();
            $('.slider').slider({
                full_width: false
            });
            $('#modal1').modal();
            $('#modal2').modal();
            $('#modal3').modal();
             $(".button-collapse").sideNav();
             Materialize.toast('Welcome!', 3000, 'rounded')
        });
    </script>
</body>

</html>
