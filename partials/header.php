<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Spup Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
    <link rel="shortcut icon" href="./media/img/spuplogo.ico" type="image/x-icon">  
    <!-Compiled and Minified CSS-!>
    <link rel="stylesheet" type="text/css" href="./lib/materialize/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="./lib/materialize/materialize-icons">
    <link rel="stylesheet" type="text/css" href="./css/mystyle.css">
    <!-Compiled and Minified Javascript-!>
    <script src="./lib/materialize/js/materialize.min.js"></script>
    <script src="./js/typed.js" type="text/javascript"></script>
    <!-Start of typed.js Script->
    <script src="./js/typed-script.js" type="text/javascript"></script>
    <!-End of typed.js Script->

    <!-Start of typed.js CSS->
    <link href="./css/typed-main.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="./css/typed-style.css">
    <!-End of typed.js CSS->
    <link rel="stylesheet" type="text/css" href="./css/animate.css">
    <!-- text slideshow JS -->
    <script src="./js/text-slider.js"></script>
</head>

<body>
<div class="container-fluid" id="top"></div>
    <div class="row" id="row1">
    <div class="col s12">
        <div class="col s6" id="content">
            <img src="./media/img/spuplogo.png" width="15%" height="auto" class="responsive-img animated rollIn">
            <h3 class="spuph3" id="spup-header-text">Saint Paul University Philippines</h3>
            <h5 class="header-text-1">Caritas.Veritas.Scientia</h5>
        </div>
        <div class="col s6">
            <img src="./media/img/mark.png" width="50%" heiht="50%" class="globally animated infinite jello responsive-img">
        </div>
    </div>
</div>
<div class="divider"><img src="./media/img/spup-nle-topnotchers.jpg"></div>

<div class="row" id="nav-wrapper">
  <nav>
    <div class="nav-wrapper z-depth-5">
      <a href="index.php" class="brand-logo"><i class="large material-icons left ">home</i>Home</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="history.php" class="waves-effect waves-yellow lighten-2 btn pulse z-depth-4 green lighten-4">History <i class="material-icons right">pages</i></a>
        </li>

        <li><a href="vision-mission.php" class="waves-effect waves-yellow lighten-2 btn pulse green lighten-4 z-depth-5">Vission & Mission <i class="material-icons right">pages</i></a>
        </li>

        <li><a href="core-values.php" class="waves-effect waves-yellow lighten-2 btn pulse z-depth-4 green lighten-3">Core Values <i class="material-icons right">pages</i></a>
        </li>

        <li><a href="hymn.php" class="waves-effect waves-yellow lighten-2 btn pulse z-depth-4 green lighten-3">Paulinian Hymn <i class="material-icons right">pages</i></a>
        </li>

      </ul>

      	<!-- mobile view navbar -->
       <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="history.php" class="waves-effect waves-yellow lighten-2 btn pulse z-depth-1 green lighten-3">History <i class="material-icons right">pages</i></a>
        </li>

        <li><a href="vision-mission.php" class="waves-effect waves-yellow lighten-2 btn pulse z-depth-2 green lighten-3">Vision Mission <i class="material-icons right">pages</i></a>
        </li>

        <li><a href="core-values.php" class="waves-effect waves-yellow lighten-2 btn pulse z-depth-3 green lighten-3">Core Values <i class="material-icons right">pages</i></a>
        </li>

        <li><a href="hymn.php" class="waves-effect waves-yellow lighten-2 btn pulse z-depth-3 green lighten-3">Paulianian Hymn <i class="material-icons right">pages</i></a>
        </li>

      </ul>
    </div>
  </nav>
  </div>