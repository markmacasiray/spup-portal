<?php include './partials/header.php' ?>
<div class="row">
		<div class="col s12">
		<div class="card">
			<div class="card-image">
				<img src="./media/img/university-banner.jpg" class="responsive-img">
			</div>
		</div>
	</div>
</div>
	<h1 class="hymn-header center-align animated infinite pulse">Paulinian <span class="header-hymn">Core Values</span></h1>
	<div class="divider"><img src="./media/img/spup-nle-topnotchers.jpg"></div>
	<div class="row">
	<div class="col s12 m12 l12" id="tabs-bg">
		 <ul id="tabs-swipe" class="tabs">
		    <li class="tab col s2"><a href="#test-swipe-1">Christ-Centeredness</a></li>
		    <li class="tab col s2"><a href="#test-swipe-2">Commission</a></li>
		    <li class="tab col s2"><a href="#test-swipe-3">Community</a></li>
		    <li class="tab col s2"><a href="#test-swipe-4">Charism</a></li>
		    <li class="tab col s2"><a href="#test-swipe-5">Charity</a></li>
		  </ul>
		  <hr>
		    <div id="test-swipe-1" class="col s12 lime lighten-1">
		    	<h5 class="tab-bg-header animated zoomInLeft">Christ Centerdness</h5>
		    	<p class="pbody">Christ is the center of Paulinian life; he/she follows and imitates Christ, doing everything in reference to Him.</p>
		    </div>

			<div id="test-swipe-2" class="col s12 lime lighten-1">
				<h5 class="tab-bg-header animated zoomInLeft">Commission</h5>
				<p class="pbody">The Paulinian has a mission - a life purpose to spread the Good News; like Christ, he/she actively works "to save" this world, to make it better place to live in.</p>
			</div>
			<div id="test-swipe-3" class="col s12 lime lighten-1">
				<h5 class="tab-bg-header animated zoomInLeft">Community</h5>
				<p class="pbody">The Paulinian is a responsible family member and citizen, concerned with building communities, promotion of people, justice, and peace, and the protection of the environment.</p>
			</div>
			<div id="test-swipe-4" class="col s12 lime lighten-1">
				<h5 class="tab-bg-header animated zoomInLeft">Charism</h5>
				<p class="pbody">The Paulinian develops his/her gifts/talents to be put in the service of the community, he/she strives to grow and improve daily, always seeking the better and finer things, and the final good.</p>
			</div>
			<div id="test-swipe-5" class="col s12 lime lighten-1">
				<h5 class="tab-bg-header animated zoomInLeft">Charity</h5>
				<p class="pbody">Urged on by the love of Christ, the Paulinian is warm, hospitable, and "all to all", especially to the underprivileged.</p>
				<br>
				<p class="pbody">Thus, Paulinian Education is committed to the formation of self-directed Catholic Filipino men and women who find fulfillment in intelligent fellowship and responsible leadership in meeting their responsibilities to God, country, and fellowmen.</p>
			</div>
	</div>
</div>
<?php include './partials/footer.php' ?>