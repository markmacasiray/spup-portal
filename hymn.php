<?php include './partials/header.php' ?>
<div class="row">
        <div class="col s12">
        <div class="card">
            <div class="card-image">
                <img src="./media/img/university-banner.jpg" class="responsive-img">
            </div>
        </div>
    </div>
</div>
    <h1 class="hymn-header center-align animated infinite pulse">Paulinian <span class="header-hymn">Hymn</span></h1>
    <div class="divider"><img src="./media/img/spup-nle-topnotchers.jpg"></div>
        <p class="z-depth-5 p-hymn" id="menu">Hark! Sons and Daughters of St. Paul,
    <br>
        Come listen to his call.
        <br>
        O children of this loved school,
        <br>
        The loving nurse of all.
        <br>
        Rejoice in God, do work and play,
        <br>
        Be true from day to day;
        <br>
        Beloved school of mine,
        <br>
        My pains and joys are thine.
        <br>
        My childhood’s early dreams are closely linked with thee;
        <br>
        The hope that heaven brings
        <br>
        Thou dost unfold to me. (2x)
        <br>
 
        Sweet are the days of childhood,
        <br>
        With friends we love and care;
        <br>
        Those golden years of childhood
        <br>
        Whose sympathy we share,
        <br>
        Do stay and while the hours away,
        <br>
        With us in work and play,
        <br>
        And when we leave our dear old school,
        <br>
        These mem’ries we’ll recall. (2x)
        <br>
 
        (Repeat Refrain)</p>
        <div class="divider"></div>
<?php include './partials/footer.php' ?>