<?php include './partials/header.php' ?>

    <div class="slider">
        <ul class="slides">
            <li>
                <img src="./media/img/welcome-back-paulinians.jpg" class="responsive-img">
            </li>

            <li>
                <img src="./media/img/isa-certificate.jpg" class="responsive-img">
            </li>

            <li>
                <img src="./media/img/r-and-r-2017.jpg" class="responsive-img">
            </li>

            <li>
                <img src="./media/img/radtech-board-2017.jpg" class="responsive-img">
            </li>

            <li>
                <img src="./media/img/spup-at-110.jpg" class="responsive-img">
            </li>

            <li>
                <img src="./media/img/spup-nle-topnotchers.jpg" class="responsive-img">
            </li>
        </ul>
    </div>
    <!-- End of slider -->
    <div class="divider"><img src="./media/img/spup-nle-topnotchers.jpg"></div>
    <div class="row">
        <div class="col s7 push s-5">
            <div class="col s4 center"><img src="./media/img/congratulations.jpg" class="responsive-img"></div>


        </div>
        <div class="col s5 pull s-7">
                
                    <div class="type-wrap">
                    <div id="typed-strings">
                    <span>SPUP is an Autonomous University and was awarded Center of excellence in <strong>Nursing</strong>, Center of Excellence in <strong>Teacher Education</strong>,Center of Development in <strong>Information Technology</strong></span>
                    <p>New Paulinian Registered<strong>Radiologic Technologists, Batch Passing Rate: 100%</strong></p>
                    </div>
                <span id="typed"></span>
                </div>
                </div>
            
        </div>
    </div>

    <div class="row">

        <div class="col s12 m6 l3">
            <h5 class="header" id="pa-achiever">Paulinian Achievers</h5>
            <hr>
            <br>

            <div class="z-depth-4">
                <h5>John Kit Masigan</h5>
                <p class="left-align">TOSP 2014</p>

                <h5>Giovanni Cabalza</h5>
                <p class="left-align">TSOP Region 02 2015 Awardee</p>

                <h5>Karylle Mae Formoso</h5>
                <p class="left-align">TOSP Region 02 2015 Awardee</p>

                <h5>Nelson Gacutan</h5>
                <p class="left-align">Outstanding UNESCO Youth Leaders of the Philippines 2013 Awardee</p>

                <h5>Mark Anthony Baliuag</h5>
                <p class="left-align">Delegate, 29th International Youth Leadership Conference in Prague, Czech Republic</p>

                <h5>Jayson Tacazon</h5>
                <p class="left-align">Top 7, 2014 Sanitary Engineering Board Examination</p>

                <h5>Eunice Ma. Angelica Herrera</h5>
                <p class="left-align">Top 5, April 2014 Librarian Licensure Examination</p>

                <h5>James Patrick Mahor</h5>
                <p class="left-align">Outstanding Paulinian Students of the Philippines 2014 Awardee</p>
            
                <h5>Vander Norikko Casibang</h5>
                <p class="left-align">Outstanding Paulinian Students of the Philippines 2014 Finalist</p>

                <h5>Davenn Bacud</h5>
                <p class="left-align">Philippine Model Congress 2014 Delegate</p>

                <h5>Piny Ellaine Cesar</h5>
                <p class="left-align">Formosa Cultural Exchange Program 2015 Participant</p>

                <h5>Cherson Bariuan</h5>
                <p class="left-align">Formosa Cultural Exchange Program 2015 Participant</p>

                
           </div>
  
        </div>


        <div class="col s12 m6 l3">
            <h5>News</h5>
            <hr>
            <br>
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-image">
                                <img src="./media/img/news/spup-invites-pnp-tuguegarao-1.jpg"><a href="#modal1" class="btn-floating btn-large btn modal-trigger halfway-fab waves-effect waves-yellow lighten-2 green darken-4 pulse"><i class="material-icons">pageview</i></a>
                            </div>
                            <div class="card-content">
                                <h5 class="header"><strong>SPUP INVITES PNP-TUGUEGARAO FOR SHS SAFETY AND SECURITY ORIENTATION</strong></h5>
                                <p>Caritas Christi urget nos!</p>
                            </div>
                        </div> 
                        <!-- Modal Structure -->
                        <div id="modal1" class="modal modal-fixed-footer">
                            <div class="modal-content">
                                <h4 class="header">SPUP INVITES PNP-TUGUEGARAO FOR SHS SAFETY AND SECURITY ORIENTATION</h4>
                                <p class="left-align"><i>2017-09-07</i><br> <span>Caritas Christi urget nos!</span></p>

                                <p>As part of the week-long Orientation Program for the Senior High School (SHS), SPUP has invited officers from the Philippine National Police (PNP) – Tuguegarao City Station on September 07, 2017, to lead the discussion on Students’ Safety and Security. This is in accordance with the advocacy of SPUP, which aims to empower students towards peace-building engagement activities.</p>

                                <p>PSI Alexander Tamang, lectured on Crime and Drug Prevention; while, PSI Ronilyn Baccay discussed the Anti-Violence Against Women and Children Act (RA 9262). Their presentation was followed by an open forum in which the students interacted by posing questions to the speakers. SHS students intently listened as the experts talked about practical approaches and responses to prevent the occurrence of crimes, abuses and violence. Moderated by Dr. Allan Peejay Lappay (Director, Alumni, External Relations and Advocacies), the interaction fostered an avenue for a cheerful yet serious discussion on how to promote safety and security especially when students are outside SPUP. PSI Tamang and PSI Baccay provided safety and security tips as they responded to the many queries of the participants.</p>

                                <p> The session ended with the recognition of the speakers and the officers of PNP-Tuguegarao City, led by Dr. Pilar Acorda (Dean, School of Arts, Sciences and Teacher Education) and Ms. Alma Quinagoran (Director, Office of Student Affairs).</p>
                              
                                <img src="./media/img/news/spup-invites-pnp-tuguegarao-1.jpg" width="50%" class="left materialboxed"> 
                                <img src="./media/img/news/spup-invites-pnp-tuguegarao-2.jpg" width="50%" class="right materialboxed">
                              


                                
                            </div>
                            <div class="modal-footer">
                                <a href="#!" class="btn-floating modal-action modal-close waves-effect waves green btn"><i class="material-icons">close</i></a>
                            </div>
                        </div>
                    </div>
                

                    <div class="col s12">
                        <div class="card">
                            <div class="card-image">
                                <img src="./media/img/news/pvcd-unesco-1.jpg"><a href="#modal2" class="btn-floating btn-large halfway-fab waves-effect waves-yellow lighten-2 green darken-4 pulse"><i class="material-icons">pageview</i></a>
                            </div>
                            <div class="card-content">
                                <h5 class="header"><strong>SPUP'S PVCD REAPS AWARDS IN UNESC CLUBS INTERNATIONAL ASSEMBLY</strong></h5>
                                <p>Caritas Christi urget nos!</p>
                            </div>
                        </div> 
                         <!-- Modal Structure -->
                        <div id="modal2" class="modal modal-fixed-footer">
                            <div class="modal-content">
                                <h4 class="header">SPUP'S PVCD REAPS AWARDS IN UNESCO CLUBS INTERNATIONAL ASSEMBLY</h4>
                                <p class="left-align"><i>2017-09-03</i><br> <span>Caritas Christi urget nos!</span></p>

                                <p>(by: Ms. Noemi Cabaddu and Dr. Allan Peejay Lappay)
                                <br>
                                The Paulinian Volunteers for Community Development (PVCD) Club was honoured as OUTSTANDING UNESCO CLUB (Education Category) and obtained a Certificate of GOOD STANDING during the annual assembly of UNESCO Clubs in the Philippines. Moreover, Mrs. Noemi Cabaddu (CES Director and PVCD Adviser) was awarded OUTSTANDING UNESCO CLUB EDUCATOR; while, Ms. Cheeni Mabbayad (PVCD President) was hailed as OUTSTANDING UNESCO CLUB YOUTH LEADER. The awards were presented during the 2017 International Assembly of Youth for UNESCO held at the Icon Hotel, Quezon City on September 01-03, 2017.
                                <br>
                                The awarding served as one of the highlights of this year’s conference organized by the National Association of UNESCO Clubs in the Philippines (NAUCP), which was participated in by more than 300 participants from the country and abroad. Mr. Jonathan Guerero (President, NAUCP) and Dr. Serafin Arviola, Jr. (Chairman, NAUCP) led the recognition of the awardees.
                                <br>
                                (SPUP's PVCD is the FIRST UNESCO-Recognized Student CLUB in REGION II and the ONLY in the St. Paul University System. It is also the ONLY RECIPIENT from the Cagayan Valley Region of this year's Outstanding award.)
                                </p>

                                <img src="./media/img/news/pvcd-unesco-1.jpg" width="50%" class="left materialboxed"> 
                                <img src="./media/img/news/pvcd-unesco-2.jpg" width="50%" class="left materialboxed">

                            </div>
                            <div class="modal-footer">
                                <a href="#!" class="btn-floating modal-action modal-close waves-effect waves green btn"><i class="material-icons">close</i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col s12">
                        <div class="card">
                            <div class="card-image">
                                <img src="./media/img/news/spup-rhwc-1.jpg"><a href="#modal3" class="btn-floating btn-large halfway-fab waves-effect waves-yellow lighten-2 green darken-4 pulse"><i class="material-icons">pageview</i></a>
                            </div>
                            <div class="card-content">
                                <h5 class="header"><strong>SPUP'S HEALTH SERVICE REACHES OUT TO RHWC</strong></h5>
                                <p>Caritas Christi urget nos!</p>
                            </div>
                        </div> 

                        <div id="modal3" class="modal modal-fixed-footer">
                            <div class="modal-content">
                                <h4 class="header">SPUP'S HEALTH SERVICES REACHES OUT TO RHWC</h4>
                                <p class="left-align"><i>2017-09-02</i><br> <span>Caritas Christi urget nos!</span></p>
                                <p>(by: Ms. Zylla Ezra Tenedero and Mr. Jaypee Talosig)
                                <br>
                                SPUP's Health Services Unit (HSU) initiated an Outreach Activity at the Regional Haven for Women and Children (RHWC) in Maddarulug, Solana on September 2, 2017. Dr. Maricon Manuel (Head, Health Services Unit) catered to the Medical needs of the residents of the center; while, Dr. Grace Tamayao and Dr. Angelica Asuncion (University Dentists) conducted Dental services to the children at the center. Ms. Badet Quiambao, Mr. Jaypee Talosig and Ms. Zylla Ezra Tenedero (University Nurses) manned the feeding program activity. Former University Nurses, Ms. Danica Lim (now working at St. Paul Hospital) and Ms. Preiyanne Kaye Ulep (now based in New Jersey, USA) extended their help by rendering their services to the Outreach Activity. The HSU Personnel also gave simple gifts to the children as advanced-presents for the Yuletide season.
                                </p>

                                <img src="./media/img/news/spup-rhwc-1.jpg" width="50%" class="left materialboxed">
                                <img src="./media/img/news/spup-rhwc-2.jpg" width="50%" class="left materialboxed">


                            </div>
                            <div class="modal-footer">
                                <a href="#!" class="btn-floating modal-action modal-close waves-effect waves green btn"><i class="material-icons">close</i></a>
                            </div>
                        </div> 
                    </div>

                </div>
        </div>
        
        

        <div class="col s12 m6 l3">
            <h5>Events</h5>
            <hr>
            <br>
            <div class="chip">
                <p><strong>October 21</strong></p>
            </div>
            <blockquote>CPD Seminars for Nurses: Tuguegarao City</blockquote>
            <img class="materialboxed tooltipped" data-position="bottom" data-delay="50" data-tooltip="Click me to view more Information" width="250" src="./media/img/events/cpd-seminars-for-nurses.jpg">
            <br>
            <br>
            <br>
            <hr>
            <br>

            <div class="chip">
                <p><strong>December 09</strong></p>
            </div>
            <blockquote>2017 Alumni Homecoming</blockquote>
            <img class="materialboxed tooltipped" data-position="bottom" data-delay="50" data-tooltip="Click me to view more Information" width="250" src="./media/img/events/alumni-homecoming-2017.jpg">
            <br>
            <br>
            <br>
            <hr>
            <br>

            <div class="chip">
                <p><strong>December 14</strong></p>
            </div>
            <blockquote>3rd SPUP International Research Conference</blockquote>
            <img class="materialboxed tooltipped" data-position="bottom" data-delay="50" data-tooltip="Click me to view more Information" width="250" src="./media/img/events/irc3.jpg">
            <br>
            <br>
            <br>
            <hr>
        </div>

         <div class="col s12 m6 l3">
            <h5>Advertisement</h5>
            <hr>
            <br>
            
    
        </div>

       
    </div>


<?php include './partials/footer.php' ?>

<!-- sample flow button wag mong i delete kung ayaw mong magulo ang buhay mo. -->
<!-- <a class="btn-floating btn-large waves-effect waves-yellow lighten-2 green darken-4  pulse btn tooltipped" data-position="right" data-delay="50" data-tooltip="Click me to view Palinian Achievers"><i class="material-icons" onclick="Materialize.showStaggeredList('#list')">view_list</i></a> -->