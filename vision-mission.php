<?php include './partials/header.php' ?>
<div class="row">
        <div class="col s12">
        <div class="card">
            <div class="card-image">
                <img src="./media/img/university-banner.jpg" class="responsive-img">
            </div>
        </div>
    </div>
</div>
        <h1 class="hymn-header center-align animated  slideInDown">Vision <span class="header-hymn">Mission</span></h1>
        <div class="row">
        <div class="col s12 m12 l12">
        <div class="center-align">
            <ul class="collapsible popout" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header"><i class="material-icons">description</i>Vision</div>
                    <div class="collapsible-body">
                        <p>St. Paul University Philippines is an internationally recognized institution dedicated to the formation of competent leaders and responsible citizens of their communities, country, and the world.</p>
                    </div>
                </li>
 
                <li>
                    <div class="collapsible-header">
                        <i class="material-icons">description</i> Mission
                    </div>
                    <div class="collapsible-body">
                        <p>Animated by the gospel and guided by the teachings of the Church, it helps to uplift the quality of life and to effect social transformation through:
                        <br>
                        1. Quality, Catholic, Paulinian formation, academic excellence, research, and community services;
                        <br>
                        2. Optimum access to Paulinian education and service in an atmosphere of compassionate caring;    and
                        <br>
                         3. responsive and innovative management processes. </p>
                    </div>
                </li>
 
                <li>
                    <div class="collapsible-header">
                        <i class="material-icons">description</i> Quality Policy
                    </div>
                    <div class="collapsible-body">
                        <h5 style="color:#1b5e20;">Caritas Christi Urget Nos!</h5>
                        <p>("St. Paul University Philippines Provides Quality, Catholic
                            Paulinian Education in a Caring Environment")</p>
                    </div>
                </li>
            </ul>  
        </div>
        </div>
        </div>
 
<?php include './partials/footer.php' ?>
